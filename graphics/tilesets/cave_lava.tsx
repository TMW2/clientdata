<?xml version="1.0" encoding="UTF-8"?>
<tileset name="cave_lava" tilewidth="32" tileheight="32" tilecount="16" columns="16">
 <image source="cave_lava.png" width="512" height="32"/>
 <tile id="15">
  <animation>
   <frame tileid="15" duration="110"/>
   <frame tileid="14" duration="110"/>
   <frame tileid="13" duration="110"/>
   <frame tileid="12" duration="110"/>
   <frame tileid="11" duration="110"/>
   <frame tileid="10" duration="110"/>
   <frame tileid="9" duration="110"/>
   <frame tileid="8" duration="110"/>
   <frame tileid="7" duration="110"/>
   <frame tileid="6" duration="110"/>
   <frame tileid="5" duration="110"/>
   <frame tileid="4" duration="110"/>
   <frame tileid="3" duration="110"/>
   <frame tileid="2" duration="110"/>
   <frame tileid="1" duration="110"/>
   <frame tileid="0" duration="110"/>
  </animation>
 </tile>
</tileset>

<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.3" name="Mick's Fish" tilewidth="32" tileheight="32" tilecount="64" columns="8">
 <image source="aquarium-fish.png" width="256" height="256"/>
 <tile id="0">
  <animation>
   <frame tileid="0" duration="150"/>
   <frame tileid="1" duration="150"/>
   <frame tileid="2" duration="150"/>
   <frame tileid="3" duration="150"/>
   <frame tileid="5" duration="1200"/>
   <frame tileid="5" duration="150"/>
   <frame tileid="5" duration="1200"/>
   <frame tileid="12" duration="150"/>
   <frame tileid="11" duration="150"/>
   <frame tileid="10" duration="150"/>
   <frame tileid="9" duration="150"/>
   <frame tileid="5" duration="150"/>
  </animation>
 </tile>
 <tile id="1">
  <animation>
   <frame tileid="5" duration="600"/>
   <frame tileid="0" duration="150"/>
   <frame tileid="1" duration="150"/>
   <frame tileid="2" duration="150"/>
   <frame tileid="3" duration="150"/>
   <frame tileid="5" duration="600"/>
   <frame tileid="5" duration="150"/>
   <frame tileid="5" duration="600"/>
   <frame tileid="12" duration="150"/>
   <frame tileid="11" duration="150"/>
   <frame tileid="10" duration="150"/>
   <frame tileid="9" duration="150"/>
   <frame tileid="5" duration="600"/>
   <frame tileid="5" duration="150"/>
  </animation>
 </tile>
 <tile id="2">
  <animation>
   <frame tileid="5" duration="1200"/>
   <frame tileid="0" duration="150"/>
   <frame tileid="1" duration="150"/>
   <frame tileid="2" duration="150"/>
   <frame tileid="3" duration="150"/>
   <frame tileid="5" duration="150"/>
   <frame tileid="12" duration="150"/>
   <frame tileid="11" duration="150"/>
   <frame tileid="10" duration="150"/>
   <frame tileid="9" duration="150"/>
   <frame tileid="5" duration="150"/>
   <frame tileid="5" duration="1200"/>
  </animation>
 </tile>
 <tile id="4">
  <animation>
   <frame tileid="5" duration="1800"/>
   <frame tileid="4" duration="150"/>
   <frame tileid="5" duration="1800"/>
   <frame tileid="5" duration="150"/>
  </animation>
 </tile>
 <tile id="8">
  <animation>
   <frame tileid="5" duration="1800"/>
   <frame tileid="5" duration="150"/>
   <frame tileid="5" duration="1800"/>
   <frame tileid="8" duration="150"/>
  </animation>
 </tile>
 <tile id="16">
  <animation>
   <frame tileid="16" duration="150"/>
   <frame tileid="17" duration="150"/>
   <frame tileid="18" duration="150"/>
   <frame tileid="19" duration="150"/>
   <frame tileid="21" duration="1200"/>
   <frame tileid="21" duration="150"/>
   <frame tileid="21" duration="1200"/>
   <frame tileid="28" duration="150"/>
   <frame tileid="27" duration="150"/>
   <frame tileid="26" duration="150"/>
   <frame tileid="25" duration="150"/>
   <frame tileid="21" duration="150"/>
  </animation>
 </tile>
 <tile id="17">
  <animation>
   <frame tileid="21" duration="600"/>
   <frame tileid="16" duration="150"/>
   <frame tileid="17" duration="150"/>
   <frame tileid="18" duration="150"/>
   <frame tileid="19" duration="150"/>
   <frame tileid="21" duration="600"/>
   <frame tileid="21" duration="150"/>
   <frame tileid="21" duration="600"/>
   <frame tileid="28" duration="150"/>
   <frame tileid="27" duration="150"/>
   <frame tileid="26" duration="150"/>
   <frame tileid="25" duration="150"/>
   <frame tileid="21" duration="600"/>
   <frame tileid="21" duration="150"/>
  </animation>
 </tile>
 <tile id="18">
  <animation>
   <frame tileid="21" duration="1200"/>
   <frame tileid="16" duration="150"/>
   <frame tileid="17" duration="150"/>
   <frame tileid="18" duration="150"/>
   <frame tileid="19" duration="150"/>
   <frame tileid="21" duration="150"/>
   <frame tileid="28" duration="150"/>
   <frame tileid="27" duration="150"/>
   <frame tileid="26" duration="150"/>
   <frame tileid="25" duration="150"/>
   <frame tileid="21" duration="150"/>
   <frame tileid="21" duration="1200"/>
  </animation>
 </tile>
 <tile id="20">
  <animation>
   <frame tileid="21" duration="1800"/>
   <frame tileid="20" duration="150"/>
   <frame tileid="21" duration="1800"/>
   <frame tileid="21" duration="150"/>
  </animation>
 </tile>
 <tile id="24">
  <animation>
   <frame tileid="21" duration="1800"/>
   <frame tileid="21" duration="150"/>
   <frame tileid="21" duration="1800"/>
   <frame tileid="24" duration="150"/>
  </animation>
 </tile>
</tileset>

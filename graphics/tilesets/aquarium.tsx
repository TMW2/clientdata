<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.3.2" name="Aquarium-Animals" tilewidth="32" tileheight="32" tilecount="72" columns="6">
 <image source="aquarium.png" width="192" height="384"/>
 <tile id="0">
  <animation>
   <frame tileid="0" duration="250"/>
   <frame tileid="1" duration="250"/>
   <frame tileid="2" duration="250"/>
   <frame tileid="3" duration="250"/>
   <frame tileid="4" duration="250"/>
   <frame tileid="5" duration="250"/>
  </animation>
 </tile>
 <tile id="6">
  <animation>
   <frame tileid="6" duration="250"/>
   <frame tileid="7" duration="250"/>
   <frame tileid="8" duration="250"/>
   <frame tileid="9" duration="250"/>
   <frame tileid="10" duration="250"/>
   <frame tileid="11" duration="250"/>
  </animation>
 </tile>
 <tile id="12">
  <animation>
   <frame tileid="12" duration="250"/>
   <frame tileid="13" duration="250"/>
   <frame tileid="14" duration="250"/>
   <frame tileid="15" duration="250"/>
   <frame tileid="16" duration="250"/>
   <frame tileid="17" duration="250"/>
  </animation>
 </tile>
 <tile id="18">
  <animation>
   <frame tileid="18" duration="250"/>
   <frame tileid="19" duration="250"/>
   <frame tileid="20" duration="250"/>
   <frame tileid="21" duration="250"/>
   <frame tileid="22" duration="250"/>
   <frame tileid="23" duration="250"/>
  </animation>
 </tile>
 <tile id="24">
  <animation>
   <frame tileid="24" duration="250"/>
   <frame tileid="25" duration="250"/>
   <frame tileid="26" duration="250"/>
   <frame tileid="27" duration="250"/>
   <frame tileid="28" duration="250"/>
   <frame tileid="29" duration="250"/>
  </animation>
 </tile>
 <tile id="30">
  <animation>
   <frame tileid="30" duration="250"/>
   <frame tileid="31" duration="250"/>
   <frame tileid="32" duration="250"/>
   <frame tileid="33" duration="250"/>
   <frame tileid="34" duration="250"/>
   <frame tileid="35" duration="250"/>
  </animation>
 </tile>
 <tile id="36">
  <animation>
   <frame tileid="36" duration="250"/>
   <frame tileid="37" duration="250"/>
   <frame tileid="38" duration="250"/>
   <frame tileid="39" duration="250"/>
   <frame tileid="40" duration="250"/>
   <frame tileid="41" duration="250"/>
  </animation>
 </tile>
 <tile id="42">
  <animation>
   <frame tileid="42" duration="250"/>
   <frame tileid="43" duration="250"/>
   <frame tileid="44" duration="250"/>
   <frame tileid="45" duration="250"/>
   <frame tileid="46" duration="250"/>
   <frame tileid="47" duration="250"/>
  </animation>
 </tile>
 <tile id="48">
  <animation>
   <frame tileid="48" duration="250"/>
   <frame tileid="49" duration="250"/>
   <frame tileid="50" duration="250"/>
   <frame tileid="51" duration="250"/>
   <frame tileid="52" duration="250"/>
   <frame tileid="53" duration="250"/>
  </animation>
 </tile>
 <tile id="54">
  <animation>
   <frame tileid="54" duration="250"/>
   <frame tileid="55" duration="250"/>
   <frame tileid="56" duration="250"/>
   <frame tileid="57" duration="250"/>
   <frame tileid="58" duration="250"/>
   <frame tileid="59" duration="250"/>
  </animation>
 </tile>
 <tile id="60">
  <animation>
   <frame tileid="60" duration="250"/>
   <frame tileid="61" duration="250"/>
   <frame tileid="62" duration="250"/>
   <frame tileid="63" duration="250"/>
   <frame tileid="64" duration="250"/>
   <frame tileid="65" duration="250"/>
  </animation>
 </tile>
 <tile id="66">
  <animation>
   <frame tileid="66" duration="250"/>
   <frame tileid="67" duration="250"/>
   <frame tileid="68" duration="250"/>
   <frame tileid="69" duration="250"/>
   <frame tileid="70" duration="250"/>
   <frame tileid="71" duration="250"/>
  </animation>
 </tile>
</tileset>

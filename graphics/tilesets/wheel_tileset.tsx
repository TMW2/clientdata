<?xml version="1.0" encoding="UTF-8"?>
<tileset name="wheel_tileset" tilewidth="512" tileheight="512">
 <image source="wheel_tileset.png" width="2560" height="512"/>
 <tile id="0">
  <animation>
   <frame tileid="0" duration="220"/>
   <frame tileid="1" duration="220"/>
   <frame tileid="2" duration="220"/>
   <frame tileid="3" duration="220"/>
   <frame tileid="4" duration="220"/>
  </animation>
 </tile>
</tileset>